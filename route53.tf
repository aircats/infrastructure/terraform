data "aws_route53_zone" "aircats" {
  name = "airon.is"
}

resource "aws_route53_record" "www" {
  zone_id = data.aws_route53_zone.aircats.zone_id
  type    = "A"
  name    = "www"

  alias {
    name                   = aws_alb.aircats.dns_name
    zone_id                = aws_alb.aircats.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ssh-1" {
  zone_id = data.aws_route53_zone.aircats.zone_id
  type    = "A"
  name    = "ssh-1"
  ttl     = "300"

  records = [
    aws_eip.meow_1.public_ip
  ]
}

resource "aws_route53_record" "ssh-2" {
  zone_id = data.aws_route53_zone.aircats.zone_id
  type    = "A"
  name    = "ssh-2"
  ttl     = "300"

  records = [
    aws_eip.meow_2.public_ip
  ]
}
