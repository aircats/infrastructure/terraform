# terraform
Provisioning AWS resources for the simple clone of [HTTP cats](https://http.cat)  

## AWS architecture details:
| File | Service |
|---|---|
| `key_pairs.tf` | EC2 Key pairs |
| `vpc.tf` | Virtual Private Cloud |
| `vpc_igw.tf` | Internet Gateway |
| `vpc_subnets.tf` | VPC Subnets |
| `security_groups.tf` | AWS Security Groups |
| `ec2.tf` | EC2 Instances |
| `ec2_eip.tf` | Elastic IPs |
| `alb.tf` | Application Load Balancer |
| `route53.tf` | Route53 |
| `acm.tf` | Amazon Certificate Manager |

## Automated deployment

Terraform pipeline will react to `git push` by running a pipeline that executes `terraform plan` then waits for manual intervention to execute `terraform apply`  
Both the `apply` and `destroy` stages have manual "gates" which require human interaction before proceeding.  
The `.gitlab-ci.yml` file has a check to determine if we want to destroy the infrastructure (will check for a `.destroy` file).