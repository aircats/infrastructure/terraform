resource "aws_acm_certificate" "cert" {
  validation_method = "DNS"
  domain_name       = "airon.is"
  subject_alternative_names = [
    "www.airon.is",
  ]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "aircats" {
  for_each = {
    for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.aircats.zone_id
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [for record in aws_route53_record.aircats : record.fqdn]
}