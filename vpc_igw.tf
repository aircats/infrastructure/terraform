resource "aws_internet_gateway" "aircats" {
  vpc_id = aws_vpc.aircats.id

  tags = merge(local.common_tags, {
    "Name" = "aircats"
  })
}
