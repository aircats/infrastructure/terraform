resource "aws_instance" "meow_1" {
  instance_type = "t2.micro"
  key_name      = aws_key_pair.aircats.key_name
  ami           = local.ubuntu_ami
  subnet_id     = aws_subnet.aircats-http-az-a.id

  vpc_security_group_ids = [
    aws_security_group.webserver.id,
  ]

  tags = merge({
    Name = "aircats-meow-1"
  }, local.common_tags)
}

resource "aws_instance" "meow_2" {
  instance_type = "t2.micro"
  key_name      = aws_key_pair.aircats.key_name
  ami           = local.ubuntu_ami
  subnet_id     = aws_subnet.aircats-http-az-b.id

  vpc_security_group_ids = [
    aws_security_group.webserver.id,
  ]

  tags = merge({
    Name = "aircats-meow-2"
  }, local.common_tags)
}
