resource "aws_subnet" "aircats-http-az-a" {
  vpc_id            = aws_vpc.aircats.id
  cidr_block        = "10.1.1.0/27"
  availability_zone = "us-east-1a"

  tags = merge(local.common_tags, {
    "Name" = "aircats AZ A"
  })
}

resource "aws_subnet" "aircats-http-az-b" {
  vpc_id            = aws_vpc.aircats.id
  cidr_block        = "10.1.1.32/27"
  availability_zone = "us-east-1b"

  tags = merge(local.common_tags, {
    "Name" = "aircats AZ B"
  })
}

resource "aws_route_table" "aircats" {
  vpc_id = aws_vpc.aircats.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.aircats.id
  }

  tags = merge(local.common_tags, {
    "Name" = "aircats Internet Route Table"
  })
}

resource "aws_route_table_association" "aircats-http-az-a" {
  subnet_id      = aws_subnet.aircats-http-az-a.id
  route_table_id = aws_route_table.aircats.id
}

resource "aws_route_table_association" "aircats-http-az-b" {
  subnet_id      = aws_subnet.aircats-http-az-b.id
  route_table_id = aws_route_table.aircats.id
}