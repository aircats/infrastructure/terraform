locals {
  ubuntu_ami = "ami-09e67e426f25ce0d7"

  common_tags = {
    "Solution"    = "HTTP Cats"
    "Environment" = "MVP"
    "Contact"     = "AP air.drive@gmail.com"
  }
}