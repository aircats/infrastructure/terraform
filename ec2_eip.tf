resource "aws_eip" "meow_1" {
  instance = aws_instance.meow_1.id
  vpc      = true
}

resource "aws_eip" "meow_2" {
  instance = aws_instance.meow_2.id
  vpc      = true
}