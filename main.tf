terraform {
  backend "http" {
      address           = "https://gitlab.com/api/v4/projects/30781791/terraform/state/production"
      lock_address      = "https://gitlab.com/api/v4/projects/30781791/terraform/state/production/lock"
      unlock_address    = "https://gitlab.com/api/v4/projects/30781791/terraform/state/production/lock"
      lock_method       = "POST"
      unlock_method     = "DELETE"
      retry_wait_min    = 5
  }
}

provider "aws" {
  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}